export let TRIPS = [
  {
    id: 1,
    name: "SERVICES",
    price_adult: 60,
    //price_child: 30,
    sub_name: "CAR STEAM WASH AND DETAILING",
    //thumb: "assets/img/trip/thumb/trip_1.jpg",
    description: "From sexy Ipanema and Copacabana, to more secluded and slightly lesser-known stretches of sand, like Prainha Beach, Brazil's Rio de Janeiro is best known for its beaches. Grab your sunscreen and Brazilian bikinis and head to the sunny shores of Rio's best beaches.",
    location: "Rio de Janeiro, Brazil",
    
  },
  {
    id: 2,
    name: "Wheels and Mud Guards Wet Steam Cleaning",
    price_adult: 90,
    //price_child: 45,
    sub_name: "Wheels and Mud Guards Wet Steam Cleaning",
    //thumb: "assets/img/trip/thumb/trip_2.jpg",
    description: "From sexy Ipanema and Copacabana, to more secluded and slightly lesser-known stretches of sand, like Prainha Beach, Brazil's Rio de Janeiro is best known for its beaches. Grab your sunscreen and Brazilian bikinis and head to the sunny shores of Rio's best beaches.",
    location: "Rio de Janeiro, Brazil",
  },
  {
    id: 3,
    name: "SERVICES",
    price_adult: 30,
    //price_child: 15,
    sub_name: "Door Jambs Wet Steam Cleaning",
    //thumb: "assets/img/trip/thumb/trip_3.jpg",
    description: "From sexy Ipanema and Copacabana, to more secluded and slightly lesser-known stretches of sand, like Prainha Beach, Brazil's Rio de Janeiro is best known for its beaches. Grab your sunscreen and Brazilian bikinis and head to the sunny shores of Rio's best beaches.",
    location: "São Paulo, Brazil",
  },
  {
    id: 4,
    name: "SERVICES",
    price_adult: 500,
    //price_child: 250,
    sub_name: "Interior Mirrors Dry Steam Cleaning",
    //thumb: "assets/img/trip/thumb/trip_4.jpg",
    description: "From sexy Ipanema and Copacabana, to more secluded and slightly lesser-known stretches of sand, like Prainha Beach, Brazil's Rio de Janeiro is best known for its beaches. Grab your sunscreen and Brazilian bikinis and head to the sunny shores of Rio's best beaches.",
    location: "Fernando de Noronha, Brazil",

  }
]
