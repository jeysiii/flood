import {Component} from "@angular/core";
import {NavController,AlertController, ToastController} from "ionic-angular";
import {LoginPage} from "../login/login";


@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html'
})
export class SettingsPage {

  constructor(public nav: NavController,public toastCtrl: ToastController, public forgotCtrl: AlertController) {
  }

  // logout
  logout() {
    let signout = this.forgotCtrl.create({
      title: 'Sign Out',
      message: "Are you sure you want to log out?",
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel');
          }
        },
        {
          text: 'OK',
          handler: data => {
            console.log('OK');
            this.nav.setRoot(LoginPage);
          }
        }
      ]
    });
    signout.present();
  }
  }

