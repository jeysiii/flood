import {Component} from "@angular/core";
import {NavController, PopoverController, NavParams} from "ionic-angular";
import {Storage} from '@ionic/storage';


import {SettingsPage} from "../settings/settings";
;
import {SearchLocationPage} from "../search-location/search-location";

import { AngularFireDatabase } from "../../../node_modules/angularfire2/database";
import { AngularFireAuth } from "../../../node_modules/angularfire2/auth";


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {

  public badge: any;
  public currency: any;
  carts;
  public transkey: any;

  items;
  // search condition
  public search = {
    name: "Rio de Janeiro, Brazil",
    date: new Date().toISOString()
  }

  constructor(public navParams: NavParams,public fire: AngularFireAuth, public afd: AngularFireDatabase,private storage: Storage, public nav: NavController, public popoverCtrl: PopoverController) {

  }

  ionViewWillEnter() {
    // this.search.pickup = "Rio de Janeiro, Brazil";
    // this.search.dropOff = "Same as pickup";
    this.storage.get('pickup').then((val) => {
      if (val === null) {
        this.search.name = "Rio de Janeiro, Brazil"
      } else {
        this.search.name = val;
      }
    }).catch((err) => {
      console.log(err)
    });
  }

  // go to result page
 

  // choose place
  choosePlace(from) {
    this.nav.push(SearchLocationPage, from);
  }

  // to go account page
  goToAccount() {
    this.nav.push(SettingsPage);
  }



}

//
