import { Component, ViewChild } from "@angular/core";
import { NavController, AlertController, ToastController, MenuController } from "ionic-angular";
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from "../../../node_modules/angularfire2/database";
import { IonicPage, NavParams,  } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { HomePage } from '../home/home';

import { LoadingProvider } from '../../providers/loading/loading';
import { LoginPage } from "../login/login";
import { Login1Page } from "../login1/login1";



@Component({
  selector: 'page-register',
  templateUrl: 'register.html'
})
export class RegisterPage {


  userData:any;
	registerData = { email:'', password:'' };
	authForm : FormGroup;
	email: AbstractControl;
	password: AbstractControl;
  passwordtype:string='password';
  passeye:string ='eye';
  constructor(public alertCtrl: AlertController ,public toastCtrl: ToastController, public fb: FormBuilder, public navCtrl: NavController, public navParams: NavParams, public afAuth: AngularFireAuth,public loadingProvider: LoadingProvider) {
  	this.authForm = this.fb.group({
      'email' : [null, Validators.compose([Validators.required])],
      'password': [null, Validators.compose([Validators.required])],
    });

        this.email = this.authForm.controls['email'];
        this.password = this.authForm.controls['password'];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }

  alert(message: string){
    this.alertCtrl.create({
      title: 'Info!',
      subTitle: message,
      buttons: ['OK']
    }).present();
  }

/*------------------
--------------------*/

// For User Register

  userRegister(registerData){
    this.loadingProvider.startLoading();
  	console.log('loginData',registerData);
  		this.afAuth.auth.createUserWithEmailAndPassword(registerData.email, registerData.password)
        .then(result => {
          console.log('result >>',result);
          this.loadingProvider.stopLoading();
          this.moveToHome(result);
        }).catch(err => {
          this.loadingProvider.stopLoading();
          console.log('err',err);
          this.presentToast(err);
        });
  }

  // Move to register page
  register(){
  	this.navCtrl.push(RegisterPage);
  }

  //Move to First Page
  moveToHome(res){
  	console.log('res',res);
  	this.navCtrl.setRoot(LoginPage,{res:res});
  }

  presentToast(err) {
  const toast = this.toastCtrl.create({
    message: err.message,
    duration: 3000,
    position: 'bottom'
  });

  toast.present();
}
presentAlert(err) {

}

managePassword() {
  if(this.passwordtype == 'password'){
    this.passwordtype='text';
    this.passeye='eye-off';
  }else{
    this.passwordtype='password';
    this.passeye = 'eye';
  }
}



public go (){
  this.navCtrl.push(Login1Page); 
}

}
