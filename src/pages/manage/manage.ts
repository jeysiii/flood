
import { IonicPage } from 'ionic-angular';

import { Component, ViewChild } from "@angular/core/";
import { Geolocation } from '@ionic-native/geolocation';

/**
 * Generated class for the ManagePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
declare var google;

@IonicPage()
@Component({
  selector: 'page-manage',
  templateUrl: 'manage.html',
})
export class ManagePage {
  @ViewChild('map') mapElement;
  map: any;
  public lat: any;
  public lang: any;
  public json: any;
  public mlat: any[] =[];
  public mlng: any[] =[];
  
  constructor(public geo: Geolocation) { }

    ionViewDidLoad(){
    
       this.geo.getCurrentPosition().then(pos => {
        this.lat = pos.coords.latitude;
        this.lang = pos.coords.longitude;
        console.log('Position :' + this.lat + '   ' + this.lang)
        this.initMap();
      }).catch( err => console.log(err));
    }
   
    initMap(){
      this.json = [
        {
         "lng": 121.257301,
         "lat": 14.51606
      },
        {
          "lng": 121.256897,
          "lat": 14.51601
     },
        {
          "lng": 121.256577,
          "lat": 14.51549
       },
        {
          "lng": 121.256943,
          "lat": 14.51549
       },
        {
          "lng": 121.256943,
          "lat": 14.51506
       },
        {
          "lng": 121.257858,
          "lat": 14.51522
       },
        {
          "lng": 121.257973,
          "lat": 14.51568
       },
       {
        "lng": 121.257843,
        "lat": 14.51445
       },
      {
        "lng":  121.258087,
        "lat":  14.51388
      },
      {
        "lng":  121.258698,
        "lat":  14.51414
      },
      {
        "lng": 121.259018,
        "lat": 14.51476
      }
      ];
      console.log(this.json);
       
      this.json.forEach(element => {
        console.log(element.lng);

        let latLng1 = new google.maps.LatLng(element.lat,element.lng);
      let mapOptions = {
        zoom: 15,
        center: latLng1,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      };
      let latLng = new google.maps.LatLng(document.getElementById('map'), mapOptions);
      
      this.map = new google.maps.Map(this.mapElement.nativeElement,mapOptions);
      let Marker: google.maps.Marker = new google.maps.Marker({
        map: this.map,
        position: latLng1,
      });
      });
      var bounds = new google.maps.LatLngBounds();
      //Real
      //var pinImage = new google.maps.MarkerImage("http://mark.journeytech.com.ph/pin_green.png");
      for (let i = 0; i < this.json.length; i++) {
        //console.log(i);
        var marker = new google.maps.Marker({
         position:  new google.maps.LatLng(this.json[i].lat, this.json[i].lng),
         scaledSize: new google.maps.Size(125, 125),
         //icon: ("../../assets/img/pin_green.png"),
         map: this.map
     });
      }
      bounds.extend(marker.position);
     
      console.log('Position :' + this.lat + '   ' + this.lang);
      //let latLng = new google.maps.LatLng(document.getElementById('map'));
     // let latLng1 = new google.maps.LatLng(this.lat,this.lang);


/*
       let latLng1 = new google.maps.LatLng(this.lat,this.lang);
      let mapOptions = {
        center: latLng1,
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      };
      let latLng = new google.maps.LatLng(document.getElementById('map'), mapOptions);
      
      this.map = new google.maps.Map(this.mapElement.nativeElement,mapOptions);
      let Marker: google.maps.Marker = new google.maps.Marker({
        map: this.map,
        position: latLng1,
      })
*/
    



      
    }

  


}