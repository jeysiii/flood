import { Component, ViewChild } from "@angular/core";
import { NavController, AlertController, ToastController, MenuController } from "ionic-angular";
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from "../../../node_modules/angularfire2/database";
import { IonicPage, NavParams,  } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { HomePage } from '../home/home';
import { RegisterPage } from '../register/register';
import { LoadingProvider } from '../../providers/loading/loading';



@Component({
  selector: 'page-login1',
  templateUrl: 'login1.html'
})
export class Login1Page {

  @ViewChild('username') user;
  @ViewChild('password') password;

  //userData:any;
	//loginData = { email:'', password:'' };
	//authForm : FormGroup;
	//email: AbstractControl;
	//password: AbstractControl;
  passwordtype:string='password';
  passeye:string ='eye';
  constructor(public fire: AngularFireAuth,public afd: AngularFireDatabase,public alertCtrl: AlertController ,public toastCtrl: ToastController, public fb: FormBuilder, public navCtrl: NavController, public navParams: NavParams, public afAuth: AngularFireAuth,public loadingProvider: LoadingProvider) {
  	//this.authForm = this.fb.group({
     // 'email' : [null, Validators.compose([Validators.required])],
     // 'password': [null, Validators.compose([Validators.required])],
    //});

      //  this.email = this.authForm.controls['email'];
       // this.password = this.authForm.controls['password'];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Login1Page');
  }

  alert(message: string){
    this.alertCtrl.create({
      title: 'Info!',
      subTitle: message,
      buttons: ['OK']
    }).present();
  }

/*------------------
--------------------*/

// For User Login
/*loginUser(loginData){
    this.loadingProvider.startLoading();
  	console.log('loginData',loginData);
  		this.afAuth.auth.signInWithEmailAndPassword(loginData.email, loginData.password)
        .then(result => {
          console.log('result >>',result);
          this.loadingProvider.stopLoading();
          this.moveToHome(result);
        }).catch(err => {
          this.loadingProvider.stopLoading();
          console.log('err',err);
          this.presentToast(err);
        });
  } */

  loginUser() {
    this.fire.auth.signInWithEmailAndPassword(this.user.value, this.password.value)
    .then(data=>{
      console.log('got data',data);
      this.alert('Login Successfull!');
      this.navCtrl.push( HomePage );
    })
  }

  // Move to register page
  register(){
  	this.navCtrl.push(RegisterPage);
  }

  //Move to Home Page
  moveToHome(res){
  	console.log('res',res);
  	this.navCtrl.setRoot(HomePage,{res:res});
  }

  presentToast(err) {
  const toast = this.toastCtrl.create({
    message: err.message,
    duration: 3000,
    position: 'bottom'
  });

  toast.present();
}
presentAlert(err) {

}

managePassword() {
  if(this.passwordtype == 'password'){
    this.passwordtype='text';
    this.passeye='eye-off';
  }else{
    this.passwordtype='password';
    this.passeye = 'eye';
  }
}
forgetpassword(){
  let forgot = this.alertCtrl.create({
    title: 'Forgot Password?',
    message: "Enter you email address to send a reset link password.",
    inputs: [
      {
        name: 'email',
        placeholder: 'Email',
        type: 'email'
      },
    ],
    buttons: [
      {
        text: 'Cancel',
        handler: data => {
          console.log('Cancel clicked');
        }
      },
      {
        text: 'Send',
        handler: data => {
          console.log('Send clicked');
          let toast = this.toastCtrl.create({
            message: 'Email was sended successfully',
            duration: 3000,
            position: 'top',
            cssClass: 'dark-trans',
            closeButtonText: 'OK',
            showCloseButton: true
          });
          toast.present();
        }
      }
    ]
  });
  forgot.present();
}

}
