import {Component} from "@angular/core";
import {NavController, NavParams, AlertController} from "ionic-angular";
//import {Storage} from '@ionic/storage';
import { AngularFireDatabase, FirebaseListObservable } from "../../../node_modules/angularfire2/database";
import { AngularFireAuth } from "../../../node_modules/angularfire2/auth";
import { HomePage } from "../home/home";
import { Booking } from "../../models/shopping-item/shopping-item.interface";

// import {SearchCarsPage} from "../search-cars/search-cars";

@Component({
  selector: 'page-search-location',
  templateUrl: 'search-location.html'
})

export class SearchLocationPage {
  public fromto: any;
  // places
  public link: any;
  public url: any;
  public items: any;
  public currency: any;
  public price = 0;
  
  public name : any;
  public tname: any;
  public ffname: any;
  
  public tprice = 0;
  public totalprice =  0;
  public finalprice =  0;
  pres: boolean;
  
  public keys: any;
  public fkeys: any;
  public fprice: any;
  public ttname: any;
  public finalname: any;
  public uid: any;
  public numprice: any;


  selectedQuestions:string[] = [];

booking = {} as Booking;

bookingRef$: FirebaseListObservable<Booking[]>

  
  constructor(public navCtrl: NavController,public forgotCtrl: AlertController,public fire: AngularFireAuth,public afd: AngularFireDatabase, public nav: NavController, public navParams: NavParams) {
    this.fromto = this.navParams.data;
    this.pres = false;
    this.getDataFromFirebase();
    

  
    this.bookingRef$ = this.afd.list('/users/' + this.fire.auth.currentUser.uid + '/Checkout');
    
  }

  // search by item
  getDataFromFirebase(){
   

    this.link = this.navParams.get('id');
    this.url = this.link
    console.log(this.link);
    this.afd.list("/myservices/" + this.url + "/addons/").subscribe(
      data =>  {
       console.log(JSON.stringify(data))
       this.items = data;
       console.log(this.items)
  
   })
   this.afd.list("/settings/").subscribe(
    data =>  {  
     console.log(JSON.stringify(data))
     this.currency = data;
    })

    
}

clickSelectBox(itemKey){
  this.price = Number(itemKey.Price);
  this.numprice = Number(this.navParams.get('num'));
  console.log(this.numprice);
  console.log(itemKey);
   const foundAt = this.selectedQuestions.indexOf(itemKey.Name + "\n");
   console.log(foundAt);
   if (foundAt >= 0) {
      this.selectedQuestions.splice(foundAt, 1);
      this.tprice =  Number((this.tprice) - (this.price)) ;
      console.log(this.tprice);
   } else {
      this.selectedQuestions.push(itemKey.Name + "\n");
      this.tprice = Number((this.tprice) + (this.price));
      this.fprice = Number(this.tprice);
      
      console.log(this.fprice);
    
  }
  console.log(this.selectedQuestions);

}



/*
clickSelectBox(itemKey){
  //console.log(itemKey);
  this.price = Number(itemKey.Price);
  this.name = String(itemKey.Name);


   const foundAt = this.selectedQuestions.indexOf(itemKey);
   //console.log(foundAt);
   if (foundAt >= 0) {

      this.selectedQuestions.splice(foundAt, 1);
    
      this.tprice = Number((this.tprice) - (this.price));
      console.log(this.tprice);

    // this.tname =  this.selectedQuestions.push(itemKey.Name) - this.tname;
      //console.log(this.selectedQuestions);

      //this.tname =String ((this.selectedQuestions.push(itemKey.Name)) - (this.tname));
      //console.log(this.tname)


   } else {
    this.selectedQuestions.push(itemKey);
      this.selectedQuestions.push(itemKey.Name);
      console.log(this.selectedQuestions);
      //this.tname = String  ((this.name) + (this.tname));

     // console.log(this.tname);
      this.tprice = Number((this.tprice) + (this.price));
      this.fprice = (this.tprice)
      console.log(this.fprice);
  }
  //console.log(this.selectedQuestions);

} */



/*
total(Price,Name) {

  this.price = Number(Price);
  this.name = String(Name);
  this.tname = "";
  
  
  if(this.pres === true) {
    //sum of total price
     console.log(this.price);
     this.tprice = Number((this.tprice) + (this.price));
     this.totalprice = this.tprice;
     this.finalprice = this.totalprice;
     console.log(this.totalprice);

    //sum of all check name 
    //this.tname = String((this.name) + (this.tname))
   
    this.tname = String((this.tname) + (this.name));
    this.finalname = this.tname;
    this.ffname = this.finalname;

    console.log(this.ffname);
     this.pres = false;
  }else{
    //decrease total
    this.tprice = Number((this.tprice) - (this.price));
    this.totalprice = this.tprice;
    
    //del name
    this.tname = String((this.name) - (this.tname));
    this.finalname = this.tname;
    console.log(this.finalname);
    console.log(this.totalprice);
    console.log("None");
    
  } 
} */


proceed(){
    
  let signout = this.forgotCtrl.create({
    
    title: 'INFO',
    message: "Proceed?",
    buttons: [
      {
        text: 'Cancel',
        handler: data => {

          console.log('Cancel');
        }
      },
      {
       
        text: 'OK',
        
        handler: data => {
         
          this.bookingRef$.push({
            item_Name: this.selectedQuestions + "\n",
            //Add_Ons: this.selectedQuestions + "\n",
            Total_Price: Number((this.fprice)+ (this.numprice)),
            Status: "Pending",
          });
          this.afd.database.ref('/allcheck' + '/checkouts/' + this.fire.auth.currentUser.uid ).set({
            item_Name: this.selectedQuestions + "\n",
            //Add_Ons: this.selectedQuestions + "\n",
            Total_Price: Number((this.fprice)+ (this.numprice)),
            Status: "Pending",
            id : this.fire.auth.currentUser.uid ,
                     
                });
          this.navCtrl.push(HomePage) 
      this.afd.list('/users/' + this.fire.auth.currentUser.uid + '/shopping_cart').remove();
        console.log('OK');
        
        console.log(this.totalprice)
        }
      }
    ]
  });
  signout.present();

}


}
