import { Component, ViewChild } from "@angular/core";
import { NavController, AlertController, ToastController, MenuController } from "ionic-angular";
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from "../../../node_modules/angularfire2/database";
import { Storage } from '@ionic/storage';

import { Login1Page } from "../login1/login1";
import { HomePage } from "../home/home";



@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {

  @ViewChild('username') user;
  @ViewChild('password') password;

  users1;
  operator1;


  inputtext: string;
inputtext1: string;
 
key: string= 'inputtext';



  constructor(public storage: Storage,public afd: AngularFireDatabase,private alertCtrl: AlertController,public nav: NavController, public forgotCtrl: AlertController, public fire: AngularFireAuth,  public menu: MenuController, public toastCtrl: ToastController,public navCtrl: NavController) {
    this.menu.swipeEnable(false);

  

    this.storage.set(this.key,this.inputtext);
    
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }
  /*   alert(message: string){
    this.alertCtrl.create({
      title: 'Info!',
      subTitle: message,
      buttons: ['OK']
    }).present();
  }*/


  // go to register page
  proceed() {
    this.nav.push(HomePage);
  }







  

}
