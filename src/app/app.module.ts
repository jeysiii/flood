import {NgModule} from "@angular/core";
import {IonicApp, IonicModule} from "ionic-angular";
import {BrowserModule} from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http';
import {IonicStorageModule} from '@ionic/storage';
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {Keyboard} from '@ionic-native/keyboard';
import {ActivityService} from "../services/activity-service";
import {TripService} from "../services/trip-service";
import {MyApp} from "./app.component";
import { ManagePage } from "../pages/manage/manage";

import { HomePage } from "../pages/home/home";
import { LoginPage } from "../pages/login/login";

import { RegisterPage } from "../pages/register/register";
import { Geolocation } from "@ionic-native/geolocation";


import { AngularFireAuthModule } from 'angularfire2/auth';

//import { ServicePage } from "../pages/service/service";
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule, AngularFireDatabase  } from 'angularfire2/database';
import { FavoriteProvider } from '../providers/favorite/favorite';
//import { AdminPage } from '../pages/admin/admin';
import { AuthProvider } from '../providers/auth/auth';

import { Login1Page } from '../pages/login1/login1';
import { LoadingProvider } from '../providers/loading/loading';
import { GoogleMapComponent } from '../components/google-map/google-map';
import { GoogleMaps } from '@ionic-native/google-maps';




const Auth = {
  apiKey: "AIzaSyB0eCCRCh7UA_agWurj5mHTii6TMr0TXuw",
  authDomain: "floodmapping-81e87.firebaseapp.com",
  databaseURL: "https://floodmapping-81e87.firebaseio.com",
  projectId: "floodmapping-81e87",
  storageBucket: "floodmapping-81e87.appspot.com",
  messagingSenderId: "173072121781"
};
//const firebaseAuth = {
 // apiKey: "AIzaSyA2TUz8WlI1HCZo9Mqb_Ku7B3FIZNeaJaM",
 // authDomain: "cwappdatabase.firebaseapp.com",
  //databaseURL: "https://cwappdatabase.firebaseio.com",
  //projectId: "cwappdatabase",
  //storageBucket: "cwappdatabase.appspot.com",
  //messagingSenderId: "879510926406"
//};



// import services
// end import services
// end import services

// import pages
// end import pages

@NgModule({
  declarations: [
    MyApp,

    //SettingsPage,
    //CheckoutTripPage,
    HomePage,
    LoginPage,
   //ServicePage,
    //NotificationsPage,
    RegisterPage,
  
    Login1Page,
    GoogleMapComponent,
    ManagePage

  


  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AngularFireDatabaseModule,
    IonicModule.forRoot(MyApp, {
      scrollPadding: false,
      scrollAssist: true,
      autoFocusAssist: false,
     
      
    }),
   // AngularFireModule.initializeApp(firebaseAuth),
      AngularFireModule.initializeApp(Auth),
      AngularFireAuthModule,
    IonicStorageModule.forRoot({
      name: '__ionic3_start_theme',
        driverOrder: ['indexeddb', 'sqlite', 'websql']
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    //SettingsPage,
    //CheckoutTripPage,
    HomePage,
    LoginPage,

   
    //NotificationsPage,
    RegisterPage,
    ManagePage,
    Login1Page,

   
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Keyboard,
    ActivityService,
    TripService,
    AngularFireDatabaseModule,
    AngularFireDatabase,
    FavoriteProvider,
    AuthProvider,
    HttpClientModule,
    LoadingProvider,
    GoogleMapComponent,
    GoogleMaps,

    Geolocation,

 

   
  ]
})

export class AppModule {
}
