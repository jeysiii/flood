import { Component, ViewChild } from "@angular/core";
import { Platform, Nav, App, AlertController, ToastController} from "ionic-angular";
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Keyboard } from '@ionic-native/keyboard';
import { HomePage } from "../pages/home/home";
import { LoginPage } from "../pages/login/login";
import { ManagePage } from "../pages/manage/manage";





export interface MenuItem {
    title: string;
    component: any;
    icon: string;
}

@Component({
  templateUrl: 'app.html'
})

export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = LoginPage;

  appMenuItems: Array<MenuItem>;

  constructor(
    public toastCtrl: ToastController,
    public forgotCtrl: AlertController,
    public app: App,
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public keyboard: Keyboard
    
  ) {
    this.initializeApp();

    this.appMenuItems = [
      {title: 'Home', component: HomePage, icon: 'ios-home'},
      {title: 'Safearea', component: ManagePage, icon: 'md-book'},
     // {title: 'Settings', component: HomePage, icon: 'ios-cog'},
      {title: 'Exit', component: LoginPage, icon: 'md-log-out'},
      
    ];
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.

      //*** Control Splash Screen
      // this.splashScreen.show();
      // this.splashScreen.hide();

      //*** Control Status Bar
      this.statusBar.styleDefault();
      this.statusBar.overlaysWebView(false);

      //*** Control Keyboard
      this.keyboard.disableScroll(true);

    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  signOutUser() {
    let signout = this.forgotCtrl.create({
      title: 'Sign Out',
      message: "Are you sure you want to log out?",
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel');
          }
        },
        {
          text: 'OK',
          handler: data => {
            console.log('OK');
            this.nav.setRoot(LoginPage);
          }
        }
      ]
    });
    signout.present();
  }
  }


