import { Component, ViewChild } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation';
/**
 * Generated class for the GoogleMapComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
declare var google;

@Component({
  selector: 'google-map',
  templateUrl: 'google-map.html'
})
export class GoogleMapComponent {
  @ViewChild("map") mapElement;
  map: any;
  public lat: any;
  public lang: any;

   constructor(public geo: Geolocation) {
   }
 ngOnInit(){
  this.geo.getCurrentPosition().then(pos => {
    this.lat = pos.coords.latitude;
    this.lang = pos.coords.longitude;
    console.log('Position :' + this.lat + '   ' + this.lang)
    this.initMap();
  }).catch( err => console.log(err));
   }

   initMap(){
    
     let coords = new google.maps.LatLng(this.lat,this.lang);
     let mapOptions: google.maps.MapOptions = {
       center: coords,
       zoom: 14,
       mapTypeId: google.maps.MapTypeId.ROADMAP
     }

     this.map = new google.maps.Map(document.getElementById('map'), mapOptions)
 
     let Marker: google.maps.Marker = new google.maps.Marker({
       map: this.map,
       position: coords,
     });
   }
 }
 