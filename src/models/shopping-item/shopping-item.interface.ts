export interface ShoppingItem {
    itemName: string;
    itemPrice: string;
    itemBadge: string;
    itemImage: string;
}
export interface UserInfo {
    FirstName: string;
    LastName: string;
    NickName: string;
    Birthday: string;
    Gender: string;
    Email: string;
    Mobile: string;
}

export interface Booking {
  item_Name: string;
  Add_Ons: string;
  item_Price: string;
  Status: string;
  id : string;
}

export interface CartingItem {
    serviceNames: string;
    totalPrice: string;
    Status: string;
  }
export interface Ulvl{
    
    email: string;
    userrole: string;
    adminrole: string;
    operator: boolean;
    reader: boolean;
}




export class User{
    userrole: string;
    adminrole: string;
    role: Ulvl;
    
    constructor(authData){
    this.userrole = authData.userrole  
}
}

